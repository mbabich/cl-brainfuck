(defpackage #:cl-brainfuck
  (:nicknames #:clbf)
  (:use #:cl #:alexandria)
  (:import-from #:zr-utils
                #:decf-mod
                #:incf-mod)
  (:export #:*tape-length*
           #:brainfuck
           #:brainfuck-reader-macro
           #:compile-brainfuck-file
           #:read-brainfuck
           #:read-brainfuck-character
           #:write-brainfuck-character))

(in-package #:cl-brainfuck)

(defvar *tape-length* 30000
  "This is the tape length, which defaults to the original Brainfuck
  implementation's length.")

;;;; Brainfuck I/O

(defun write-brainfuck-character (brainfuck-number &optional (stream *standard-output*))
  "Writes a standard Brainfuck character, which is a number from 0 to
127, to a stream. This will only work properly on Common Lisps that
use ASCII or Unicode."
  (declare ((unsigned-byte 8) brainfuck-number))
  (if (> brainfuck-number 127)
      (error "Invalid character!")
      (write-char (code-char brainfuck-number) stream)))

(defun read-brainfuck-character (&optional (stream *standard-input*))
  "Reads a character from a stream and converts it to a standard
Brainfuck character, which is a number from 0 to 127."
  (let ((input (char-code (read-char stream nil (code-char 0)))))
    (if (<= 0 input 127)
        input
        (error "This Brainfuck only supports ASCII!"))))

(defun combine-four-bytes (byte-0 byte-1 byte-2 byte-3)
  (declare ((unsigned-byte 8) byte-0 byte-1 byte-2 byte-3)
           (optimize (speed 3)))
  (logior (ash byte-0 24) (ash byte-1 16) (ash byte-2 8) byte-3))

;;;; Brainfuck reader

(eval-when (:compile-toplevel :load-toplevel :execute)
  (declaim (inline brainfuck-to-lisp))
  (defun brainfuck-to-lisp (char char-count stream
                            &key
                              in-reader-macro-p
                              (debug-p t)
                              extended-p)
    "Converts a Brainfuck character to the equivalent Lisp source."
    (cond ((char= char #\<) (list `(decf-mod tape-position tape-length ,char-count)))
          ((char= char #\>) (list `(incf-mod tape-position tape-length ,char-count)))
          ((char= char #\+) (list `(incf-mod (aref tape tape-position) 256 ,char-count)))
          ((char= char #\-) (list `(decf-mod (aref tape tape-position) 256 ,char-count)))
          ((char= char #\.) (loop for i below char-count
                               collect
                                 `(write-brainfuck-character (aref tape tape-position) output-stream)))
          ((char= char #\,) (loop for i below char-count
                               collect
                                 `(setf (aref tape tape-position)
                                        (the (mod 128) (read-brainfuck-character input-stream)))))
          ((char= char #\[) (list `(do nil
                                       ((zerop (aref tape tape-position)))
                                     ,@(read-brainfuck stream
                                                       :in-loop-p t
                                                       :in-reader-macro-p in-reader-macro-p
                                                       :debug-p debug-p
                                                       :extended-p nil))))
          ;; fixme: error on having too many ]s at the end, which can
          ;; be done by counting how deep it is into a loop instead of
          ;; just whether it is in a loop
          ((char= char #\]) (list))
          ((and debug-p (char= char #\#)) (list `(break)))
          ;; Extensions to Brainfuck
          ((and extended-p (char= char #\{) (list `(decf-mod tape-position
                                                             tape-length
                                                             (the (unsigned-byte 32)
                                                                  (combine-four-bytes (aref tape tape-position)
                                                                                      (aref tape (mod (1+ tape-position) tape-length))
                                                                                      (aref tape (mod (+ 2 tape-position) tape-length))
                                                                                      (aref tape (mod (+ 3 tape-position) tape-length))))))))
          ((and extended-p (char= char #\}) (list `(incf-mod tape-position
                                                             tape-length
                                                             (the (unsigned-byte 32)
                                                                  (combine-four-bytes (aref tape tape-position)
                                                                                      (aref tape (mod (1+ tape-position) tape-length))
                                                                                      (aref tape (mod (+ 2 tape-position) tape-length))
                                                                                      (aref tape (mod (+ 3 tape-position) tape-length))))))))
          ((and extended-p (char= char #\@) (list `(setf tape-position 0))))))

  (defun read-brainfuck (stream
                         &key
                           in-loop-p
                           in-reader-macro-p
                           (debug-p t)
                           extended-p)
    "Reads Brainfuck from stream, and places it either at the top
level or within a Brainfuck while loop."
    (do ((list (list))
         (char (if (or in-loop-p in-reader-macro-p)
                   (read-char stream)
                   (read-char stream nil :eof))
               (if (or in-loop-p in-reader-macro-p)
                   (read-char stream)
                   (read-char stream nil :eof)))
         (char-count 1))
        ((or (and (not in-loop-p) (not in-reader-macro-p) (eql char :eof))
             (and in-loop-p (char= char #\]))
             ;; fixme: If in-reader-macro-p, count the left
             ;; parentheses, and do not close unless it's at the top
             ;; level.
             (and in-reader-macro-p (char= char #\))))
         list)
      ;; This optimization counts repeated characters so increments
      ;; and decrements can be done more than one at a time.
      ;;
      ;; Fixme: comments (i.e. all ignored characters) ruin this
      ;; optimization
      (unless (char= char #\[)
        (do ((repeated-char char (read-char stream)))
            ((char/= repeated-char (peek-char nil stream nil (code-char 0))))
          (incf char-count)))
      (setf list (nconc list (brainfuck-to-lisp char
                                                char-count
                                                stream
                                                :in-reader-macro-p in-reader-macro-p
                                                :debug-p debug-p
                                                :extended-p extended-p)))
      (setf char-count 1))))

;;;; Brainfuck

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun initialize-brainfuck (stream
                               &key
                                 (tape-length *tape-length*)
                                 in-reader-macro-p
                                 (debug-p t)
                                 extended-p)
    "Creates the Brainfuck runtime for a Brainfuck program and then
reads in the Brainfuck program from a stream."
    `(lambda (&key
           (input-stream *standard-input*)
           (output-stream *standard-output*))
       (declare (ignorable input-stream output-stream))
       (let ((tape (make-array ,tape-length
                               :element-type '(unsigned-byte 8)
                               :initial-element 0))
             (tape-length ,tape-length)
             (tape-position 0))
         (declare (ignorable tape-length))
         (declare ((mod ,tape-length) tape-position))
         ,@(read-brainfuck stream
                           :in-reader-macro-p in-reader-macro-p
                           :debug-p debug-p
                           :extended-p extended-p)
         nil))))

;;; todo: define a package that uses cl-brainfuck instead of being in
;;; the package cl-brainfuck
(defun brainfuck-file-to-lisp-file (input-path output-path
                                    &key
                                      (tape-length *tape-length*)
                                      (if-exists :error)
                                      (debug-p t)
                                      extended-p)
  (check-type tape-length fixnum)
  (let* ((brainfuck-input `(defun main ()
                             (,(with-input-from-file (input-file-stream input-path)
                                 (initialize-brainfuck input-file-stream
                                                       :tape-length tape-length
                                                       :debug-p debug-p
                                                       :extended-p extended-p)))))
         (*readtable* (copy-readtable nil)))
    (setf (readtable-case *readtable*) :invert)
    (with-open-file (output-file-stream output-path :direction :output :if-exists if-exists)
      (write `(in-package #:cl-brainfuck) :stream output-file-stream)
      (terpri output-file-stream)
      (terpri output-file-stream)
      (write brainfuck-input :stream output-file-stream)
      (terpri output-file-stream)
      (terpri output-file-stream)
      (write '(main) :stream output-file-stream)
      (terpri output-file-stream))))

(defun compile-brainfuck-file (input-path output-path
                               &key
                                 (tape-length *tape-length*)
                                 (if-exists :error)
                                 (debug-p t)
                                 extended-p)
  "Turns a brainfuck file located at input-path into a Lisp file
located at output-path and then compiles that Lisp file to produce a
compiled program. The return values are equal to the return values of
compile-file, which means that the primary value is the
implementation-specific path to the compiled source."
  (brainfuck-file-to-lisp-file input-path
                               output-path
                               :tape-length tape-length
                               :if-exists if-exists
                               :debug-p debug-p
                               :extended-p extended-p)
  (compile-file output-path))

(defmacro brainfuck (string
                     &key
                       (tape-length *tape-length*)
                       (debug-p t)
                       extended-p)
  "Produces Lisp equivalent to a string of Brainfuck source code. It's
only slightly harder to understand than FORMAT."
  (check-type tape-length fixnum)
  (with-input-from-string (stream string)
    (initialize-brainfuck stream
                          :tape-length tape-length
                          :debug-p debug-p
                          :extended-p extended-p)))

(defun read-brainfuck-from-reader-macro (stream char count)
  (declare (ignore char))
  (if (null count) (setf count *tape-length*))
  (check-type count fixnum)
  (let ((first-char (read-char stream)))
    (if (char= first-char #\()
        ;; fixme: configure?
        (initialize-brainfuck stream
                              :tape-length count
                              :in-reader-macro-p t
                              :debug-p t
                              :extended-p t)
        (error "Brainfuck in a reader macro needs to be parenthesized."))))

(defun brainfuck-reader-macro ()
  "Provides a reader macro that dispatches on #f or #if for some
fixnum i. The brainfuck must be parenthesized. The numerical prefix is
the size of the tape."
  (make-dispatch-macro-character #\# #\f)
  (set-dispatch-macro-character #\# #\f #'read-brainfuck-from-reader-macro))
