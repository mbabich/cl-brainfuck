(defpackage #:cl-brainfuck-examples
  (:nicknames #:clbf-examples)
  (:use #:cl #:cl-brainfuck)
  (:import-from #:zr-utils
                #:character-pipe)
  (:export #:compile-hello-world
           #:hello-world
           #:hello-world-between-brainfucks
           #:hello-world-explained
           #:hello-world-via-reader))

(in-package #:cl-brainfuck-examples)

;;;; Sample programs

(eval-when (:compile-toplevel :load-toplevel :execute)
  (brainfuck-reader-macro))

(defun hello-world ()
  (funcall (brainfuck "+++++++++[>++++++++<-]>.<+++[>++++++++<-]>+++++.+++++++..+++.>++++[>++++++++<-]>.
                       <<<+++[>--------<-]>.<+++[>++++++++<-]>.+++.------.--------.>>+.<++++++++++."
                      :tape-length 4)))

(defun hello-world-via-reader ()
  (#4f(+++++++++[>++++++++<-]>.<+++[>++++++++<-]>+++++.+++++++..+++.>++++[>++++++++<-]>.
       <<<+++[>--------<-]>.<+++[>++++++++<-]>.+++.------.--------.>>+.<++++++++++.)))

(defun hello-world-between-brainfucks ()
  (let ((brainfuck-1 #4f(+++++++++[>++++++++<-]>.<+++[>++++++++<-]>+++++.+++++++..+++.>++++[>++++++++<-]>.
                         <<<+++[>--------<-]>.<+++[>++++++++<-]>.+++.------.--------.>>+.<++++++++++.))
        (brainfuck-2 #1f(,[.,]))
        (pipe (make-instance 'character-pipe)))
    (funcall brainfuck-1 :output-stream pipe)
    (funcall brainfuck-2 :input-stream pipe)))

(defun hello-world-explained ()
  ;; Think in octal ASCII character codes and pairs of cells, one
  ;; being used by the while loop and the other holding the
  ;; value.
  ;;
  ;; Each line with []s is a loop starting in one of the two
  ;; pairs (if the line starts in the value half, the first step is
  ;; moving left).
  ;;
  ;; If there's more than 8 but fewer than 16, a loop isn't necessary,
  ;; but spaces will be used separate the +s or -s into groups of 8.
  (#4f(+ ++++++++ [> ++++++++ < -] > . H
       < +++ [> ++++++++ < -] > +++++ . e
       +++++++ .. ll
       +++ . o
       >
       ++++ [> ++++++++ < -] > . space
       <<
       < +++ [> -------- < -] > . W
       < +++ [> ++++++++ < -] > . o
       +++ . r
       ------ . l
       -------- . d
       >>
       + . exclamation mark
       <
       ++ ++++++++ . newline)))

;;; And now it's time for a practical example!
;;;
;;; Returns t if true and nil if false. This is a famous matter of
;;; style in CL with many solutions including (if object t nil) and
;;; (not (not object)) and now this. Essentially, this turns a
;;; generalized Boolean into a Boolean.
;;;
;;; There are more concise solutions in Brainfuck, but this is
;;; designed to be very readable!
(defun coerce-to-boolean (object)
  (let* (;; Takes in the printed representation of an object and
         ;; prints the null character if there is a match and
         ;; otherwise prints some other character.
         (program #8f(>
                      ;; Sets the expected string value using octal
                      >> + ++++++++ [< ++++++++ > -] < ++++++ ; 116 N
                      >> + ++++++++ [< ++++++++ > -] < +      ; 111 I
                      >> + ++++++++ [< ++++++++ > -] < ++++   ; 114 L
                      >                                       ;   0
                      <<<<
                      ;; Subtracts the input from the expected value
                      ,[   > - <    -]
                      ,[  >> - <<   -]
                      ,[ >>> - <<<  -]
                      ,[>>>> - <<<< -]
                      ;; Any nonzero cell increments the result cell
                      > [   << + >>    [-]]
                      > [  <<< + >>>   [-]]
                      > [ <<<< + >>>>  [-]]
                      > [<<<<< + >>>>> [-]]
                      ;; The result cell is only zero if all four
                      ;; subtractions resulted in a zero
                      <<<<< .))
         (output (with-output-to-string (output)
                   (with-input-from-string (input (format nil "~S" object))
                     (funcall program
                              :input-stream input
                              :output-stream output)))))
    ;; If the program has the null character as its only output, then
    ;; it's a match.
    (not (and (= 1 (length output))
              (char= (char output 0) (code-char 0))))))

(defun compile-hello-world ()
  (let* ((examples-location (asdf:system-source-directory "cl-brainfuck"))
         (hello-bf (merge-pathnames examples-location #P"hello.bf"))
         (hello-lisp (merge-pathnames examples-location #P"hello.lisp")))
    (compile-brainfuck-file hello-bf
                            hello-lisp
                            :if-exists :supersede)))
