# Extensions to Brainfuck
When debug is enabled, there is one additional command:
 - # brings up the CL debugger via (break), allowing one to inspect
   the local variables, including the tape

When extensions are enabled, there are several additional commands:
 - { moves left by the amount specified by four adjacent cells
   starting at the current cell, treating each cell as an
   (unsigned-byte 8)

 - } moves right by the amount specified by four adjacent cells
   starting at the current cell, treating each cell as an
   (unsigned-byte 8)

 - @ returns the tape position to the origin

# Brainfuck I/O
There should be an end of file convention. The most common ones are to
set the cell to 0 or -1 or to leave it unchanged. Perhaps all three
should be offered, as options. Right now 0 is used as the end of file
value because it is the easiest to implement with the current code.

# Brainfuck reader
Brainfuck implementations occasionally also use the symbol !

# Brainfuck
Optimize [-], which zeros a cell.

Optimize the very simple loops where an increment or decrement
instruction on a cell known to be zero is followed by a simple while
loop that goes to another cell, increments or decrements that cell,
and returns to the originally-zero cell to do one increment/decrement
in the opposite direction of the initial operation on the
originally-zero cell. This is equivalent to multiplication, and when
there are more instructions, it still does not need to modify the zero
cell.

Examples of the simple case that are multiplication:
```brainfuck
+++++ [< ++++++++ > -]
---- [< ++++ > +]
++++++ [< ---- > -]
+++ [> + < -]
```

Example of a similar case that does not need to modify the original
cell:
```brainfuck
+++ [> + . < -]
```

And if it looks like this with a shift at the end, too, you don't even
need to be on the cell at all. Just prove that it's zero first:
```brainfuck
+++++ ++++++++ [< ++++++++ > -] <
```

This optimization is pretty necessary because of the way Brainfuck is
written here for convenience.

# Brainfuck loops
Remove loops if they're known not to be executable. They're used as
block comments.

# Brainfuck syntactic sugar
Replace the syntactic sugar { and } characters with text macros that
generate actual, portable Brainfuck. (And, if necessary, change the
semantics to make sure it is actually syntactic sugar.) Optimize that
the generated Brainfuck to still use the more efficient CL code upon
detecting that pattern instead of actually using that generated
Brainfuck.

# Brainfuck macro
Copy FORMAT, where the output stream comes before the string with nil
as return directly, t as \*standard-output*, and otherwise the as the
given stream.

That is, do this:

```common-lisp
(brainfuck t ",.")
```

And optionally, do this:

```common-lisp
(brainfuck nil ",.,.,." #\h #\i #\!) => "hi!"
```

Notice how Brainfuck is only marginally less readable than format.
