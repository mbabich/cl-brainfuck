cl-brainfuck
============

This is a complete Brainfuck implementation for Common Lisp.

FAQ
===

Q. Is this a joke?

A. `+++++++++[>++++++++<-]>++++++.<+++[>++++++++<-]>+++++++++.++++++++++>++++[>++++++++<-]>.`
   `<<<+++[>--------<-]>+++++++++.+++++.----.------.`
