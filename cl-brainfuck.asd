(asdf:defsystem #:cl-brainfuck
  :description "Compile Brainfuck to Common Lisp"
  :author "Michael Babich"
  :license "MIT"
  :depends-on (:alexandria
               :zr-utils)
  :components ((:file "brainfuck")
               (:file "brainfuck-examples")))
